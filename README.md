Fichiers numériques d’impression 3d, pour créer des cales à destination des tablettes à insérer dans les supports du musée numérique Micro-Folie, configuré par La Villette.

Adaptés pour la configuration :
- Tablette : Samsung Galaxy Tab A 2016 10.1" SM-T580 16 Go
- Socle sécurisé : KIMEX 091-2054K

Fichiers bruts disponibles et éditables à l'aide du logiciel libre [FreeCAD](https://www.freecadweb.org) pour ajustements.

Un assemblage de cales dans le trancheur, formant un ensemble tablette/socle requière :
- Deux cales cale_tablette_micro-folie_largeur.obj
- Une cale cale_tablette_micro-folie_longueur_bas.obj (propose une encoche pour la caméra)
- Une cale cale_tablette_micro-folie_longueur_haut.obj

Lorsque imprimées, les 4 cales s'assemblent en s’emboîtant. L’ensemble des arêtes sont chanfreinées pour limiter les effets de bavures.

Note : Il est conseillé d’ajouter du remplissage à hauteur de 50 % au niveau des crans pour superposition de pièces, ainsi que les entourages sur approximativement 1 cm de longueur de chaque côté. Le reste de la pièce peut conserver un remplissage de 20 %.
Les trancheurs modernes permettent cela facilement (Exemple PrusaSlicer et sa fonction « Modificateur »).
